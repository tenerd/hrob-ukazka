<?php

namespace AppBundle\Form;

use AppBundle\Document\EntryRunner;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EntryRunnerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullName', TextType::class, ['label' => 'Jméno'])
            ->add('si', TextType::class, ['label' => 'Číslo SI čipu', 'required' => false])
            ->add('rentSi', CheckboxType::class, ['label' => 'Zapůjčit SI (50 Kč)', 'required' => false])
            ->add('country', CountryType::class, ['label' => 'Země', 'preferred_choices' => ['CZ', 'SK']])
            ->add('dateOfBirth', BirthdayType::class, ['placeholder' => [
                'year' => 'Rok', 'month' => 'Měsíc', 'day' => 'Den',
            ], 'label' => 'Datum narození'])
            ->add('accommodation', CheckboxType::class, ['label' => 'Ubytování v tělocvičně (160 Kč/dvě noci)', 'required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => EntryRunner::class
        ));
    }
}
