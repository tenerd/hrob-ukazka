<?php

namespace AppBundle\Form;

use AppBundle\Document\Entry;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EntryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('teamName', TextType::class, ['label' => 'Název týmu'])
            ->add('firstRunner', EntryRunnerType::class, ['label' => 'První závodník'])
            ->add('secondRunner', EntryRunnerType::class, ['label' => 'Druhý závodník'])
            ->add('category', ChoiceType::class, ['label' => 'Kategorie', 'choices' => Entry::$categories, 'choices_as_values' => true])
            ->add('categoryPrichozi', ChoiceType::class, ['label' => 'Příchozí kategorie', 'choices' =>
                [
                    'Sobota i neděle (600 Kč)' => 'both',
                    'Jen sobota (300 Kč)' => 'so',
                    'Jen neděle (300 Kč)' => 'ne'
                ], 'choices_as_values' => true])
            ->add('email', EmailType::class, ['label' => 'Kontaktní email'])
            ->add('note', TextareaType::class, ['label' => 'Poznámka pořadatelům', 'required' => false])
            ->add('submit', SubmitType::class, ['label' => 'Pokračovat na rekapitulaci přihlášky', 'attr' => ['class' => 'btn-primary']]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Entry::class
        ));
    }
}
