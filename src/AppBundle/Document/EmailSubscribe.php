<?php

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use MongoId;

/**
 * @MongoDB\Document
 */
class EmailSubscribe
{
    /**
     * @MongoDB\Id
     * @var MongoId
     */
    public $id;

    /**
     * @\Symfony\Component\Validator\Constraints\Email()
     * @MongoDB\Field(type="string")
     */
    public $email;

    /**
     * @var \DateTime
     * @MongoDB\Date
     */
    public $dateAdded;

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    public $ip;

    /**
     * @MongoDB\Field(type="string")
     * @var string user agent
     */
    public $userAgent;
}