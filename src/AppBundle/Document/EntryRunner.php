<?php


namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @EmbeddedDocument
 */
class EntryRunner
{
    /**
     * Full name of runner
     * @var string
     * @Field(type="string")
     */
    public $fullName;
    /**
     * flag if SI is rented
     * @var bool
     * @Field(type="bool")
     */
    public $rentSi = false;

    /**
     * @var null
     * @Field(type="string")
     */
    public $si;

    /**
     * @var null
     * @Field(type="string")
     */
    public $country;

    /**
     * @var \DateTime 
     * @Field(type="date")
     */
    public $dateOfBirth;

    /**
     * @var @Field(type="bool")
     */
    public $accommodation;
}