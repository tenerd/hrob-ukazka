<?php


namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbedOne;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @Document
 */
class Entry
{

    /**
     * UID of team - for pairing of the payment
     * @var string
     *
     * @Id(strategy="INCREMENT")
     */
    public $uid;

    /**
     * Name of the team
     * @var string
     * @Field(type="string")
     * @Assert\NotBlank()
     */
    public $teamName;

    /**
     * first runner
     * @var EntryRunner
     * @EmbedOne(targetDocument="AppBundle\Document\EntryRunner")
     */
    public $firstRunner;

    /**
     * second runner
     * @var EntryRunner
     * @EmbedOne(targetDocument="AppBundle\Document\EntryRunner")
     */
    public $secondRunner;

    /**
     * category
     * @var string
     * @Field(type="string")
     */
    public $category;

    /**
     * category prichozi - both days, saturday only, sunday only
     * @var string
     * @Field(type="string")
     */
    public $categoryPrichozi;

    /**
     * confirmation email
     * @var
     * @Field(type="string")
     * @Assert\Email()
     */
    public $email;

    /**
     * note
     * @var string
     * @Field(type="string")
     */
    public $note;

    /**
     * hash for changes
     * @Field(type="string")
     * @var string
     */
    public $hash;

    /**
     * @var string
     * @Field(type="string")
     */
    public $totalPrice;

    /**
     * state of entry - entered/paid
     * @var
     * @Field(type="string")
     */
    public $state = 'entered';

    /**
     * @var \DateTime
     * @Field(type="date")
     */
    public $dateAdded;

    /**
     * @Field(type="string")
     * @var string
     */
    public $ip;

    /**
     * @Field(type="string")
     * @var string user agent
     */
    public $userAgent;

    public function getCategoryName()
    {
        $categories = array_flip(Entry::$categories);
        return $categories[$this->category];
    }

    public static $categories = [
        'HH (dvojice mužů, bez omezení věku)' => 'HH',
        'DD (dvojice žen, bez omezení věku)' => 'DD',
        'HD (smíšená dvojice, bez omezení věku)' => 'HD',
        'HH40 (dvojice mužů, oba z dvojice narozeni dříve než 5.11.1976)' => 'HH40',
        'DD40 (dvojice žen, obě z dvojice narozeny dříve než 5.11.1976)' => 'DD40',
        'HD40 (smíšená dvojice, oba z dvojice narozeni dříve než 5.11.1976)' => 'HD40',
        'HH20 (dvojice mužů, oba z dvojice narozeni později než 4.11.1996)' => 'HH20',
        'DD20 (dvojice žen, obě z dvojice narozeny později než 4.11.1996)' => 'DD20',
        'HD20 (smíšená dvojice, oba z dvojice narozeni později než 4.11.1996)' => 'HD20',
        'P17 (příchozí, délka trati cca 17km)' => 'P17',
        'P12 (příchozí, délka trati cca 12km)' => 'P12',
    ];

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        $veteran = new \DateTime();
        $veteran->setDate(1976, 11, 5);

        $junior = new \DateTime();
        $junior->setDate(1996, 11, 4);

        $dob1 = $this->firstRunner->dateOfBirth;
        $dob2 = $this->secondRunner->dateOfBirth;

        switch ($this->category) {
            case 'HH40':
            case 'DD40':
            case 'HD40':
                if ($dob1 > $veteran) {
                    $context->buildViolation(
                        'Závodník "' . $this->firstRunner->fullName . '" není narozen dříve než 5.11.1976 a nemůže startovat ve veteránské kategorii ' . $this->category . '. Zvolte prosím jinou kategorii'
                    )
                        ->atPath('category')
                        ->addViolation();
                }
                if ($dob2 > $veteran) {
                    $context->buildViolation(
                        'Závodník "' . $this->secondRunner->fullName . '" není narozen dříve než 5.11.1976 a nemůže startovat ve veteránské kategorii ' . $this->category . '. Zvolte prosím jinou kategorii'
                    )
                        ->atPath('category')
                        ->addViolation();
                }
                break;
            case 'HH20':
            case 'DD20':
            case 'HD20':
                if ($dob1 < $junior) {
                    $context->buildViolation(
                        'Závodník "' . $this->firstRunner->fullName . '" je narozen dříve než 4.11.1996 a nemůže startovat v juniorské kategorii ' . $this->category . '. Zvolte prosím jinou kategorii'
                    )
                        ->atPath('category')
                        ->addViolation();
                }
                if ($dob2 < $junior) {
                    $context->buildViolation(
                        'Závodník "' . $this->secondRunner->fullName . '" je narozen dříve než  4.11.1996 a nemůže startovat v juniorské kategorii ' . $this->category . '. Zvolte prosím jinou kategorii'
                    )
                        ->atPath('category')
                        ->addViolation();
                }
                break;

            default:
                // do nothing
        }

        // validate SI / rent SI
        foreach (['firstRunner' => $this->firstRunner, 'secondRunner' => $this->secondRunner] as $path=>$runner) {
            if ($runner->si) {
                if ($runner->rentSi) {
                    // choose one
                    $context->buildViolation(
                        'Pokud chcete zapůjčit SI, nevyplňujte prosím pole Číslo SI čipu'
                    )
                        ->atPath("$path.si")
                        ->addViolation();
                }
                // si not checked -> we are ok
            } else {
                if (!$runner->rentSi) {
                    $context->buildViolation(
                        'Vyplňte prosím Číslo SI čipu nebo zaškrtněte půjčení'
                    )
                        ->atPath("$path.si")
                        ->addViolation();
                }
                // rent si checked -> ok
            }
        }
    }
}