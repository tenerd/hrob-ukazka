<?php

namespace AppBundle\Controller;

use AppBundle\Document\EmailSubscribe;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage", schemes="%httpProtocol%")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', []);
    }

    /**
     * @Route("/rozpis", name="rozpis", schemes="%httpProtocol%")
     */
    public function rozpisAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/rozpis.html.twig', []);
    }

    /**
     * @Route("/prihlasky", name="prihlasky", schemes="%httpProtocol%")
     */
    public function prihlaskyAction(Request $request)
    {

        $emailSubscribe = new EmailSubscribe();

        $form = $this->createFormBuilder($emailSubscribe)
            ->add('email', EmailType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $emailSubscribe->dateAdded = new \DateTime();
            $emailSubscribe->ip = $request->getClientIp();
            $emailSubscribe->userAgent = $request->headers->get('User-Agent');

            // insert into mongo
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($emailSubscribe);
            $dm->flush();

            
            // add to mailchimp
            $listId = '343249691a';
            $mailChimp = $this->get('mailchimp');
            $mailChimp->verify_ssl = false;
            $ret = $mailChimp->post("lists/$listId/members", [
                'email_address' => $emailSubscribe->email,
                'status'        => 'subscribed',
                'language'      => $request->getLocale()
            ]);

            if ($ret === false) {
                $this->get('logger')->error('Mailchimp error', ['err' => $mailChimp->getLastError()]);
            }

            $this->addFlash(
                'success',
                'Děkujeme za váš zájem, na email ' . $emailSubscribe->email . ' vás budeme informovat o spuštění přihlášek a dalších událostech'
            );
            return $this->redirectToRoute('prihlasky');
        }

        return $this->render('default/prihlasky.html.twig', ['form' => $form->createView()]);
    }
}
