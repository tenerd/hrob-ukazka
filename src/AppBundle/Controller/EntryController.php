<?php

namespace AppBundle\Controller;

use AppBundle\Document\EmailSubscribe;
use AppBundle\Document\Entry;
use AppBundle\Document\EntryRunner;
use AppBundle\Form\EntryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class EntryController extends Controller
{
    /**
     * @Route("/prihlaska", name="entry")
     */
    public function indexAction(Request $request)
    {
        if ($request->getSession()->has('entry')) {
            $entry = $request->getSession()->get('entry');
        } else {
            $entry = new Entry();
        }

        $form = $this->createForm(EntryType::class, $entry);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // save to session and go to recapitulation
            $request->getSession()->set('entry', $entry);
            return $this->redirectToRoute('entry-recapitulation');
        }

        return $this->render('default/prihlaska.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/prihlaska-rekapitulace", name="entry-recapitulation")
     */
    public function recapitulationAction(Request $request)
    {
        if (!$request->getSession()->has('entry')) {
            // no entry in session
            return $this->redirectToRoute('entry');
            /** @var Entry $entry */
        }
        /** @var Entry $entry */
        $entry = $request->getSession()->get('entry');
        
        $price = [];
        $totalPrice = 0;

        if ($entry->category == 'P12' || $entry->category == 'P17') {
            switch ($entry->categoryPrichozi) {
                case 'both':
                    $price[] = ['600', "Startovné příchozí (sobota i neděle): " . $entry->getCategoryName()];
                    $totalPrice = 600;
                    break;
                case 'so':
                    $price[] = ['300', "Startovné příchozí (jen sobota): " . $entry->getCategoryName()];
                    $totalPrice = 300;
                    break;
                case 'ne':
                    $price[] = ['300', "Startovné příchozí (jen neděle): " . $entry->getCategoryName()];
                    $totalPrice = 300;
                    break;
            }

        } else {
            $price[] = ['600', "Startovné kategorie: " . $entry->getCategoryName()];
            $totalPrice = 600;
        }

        foreach ([$entry->firstRunner, $entry->secondRunner] as $runner) {
            /** @var $runner EntryRunner */
            if ($runner->rentSi) {
                $price[] = ['50', "Půjčovné SI"];
                $totalPrice += 50;
            }
            if ($runner->accommodation) {
                $price[] = ['160', "Ubytování v tělocvičně"];
                $totalPrice += 160;
            }
        }


        $form = $this->createFormBuilder()
            ->add('submit', SubmitType::class, ['label' => "Potvrzuji přihlášku", 'attr' => ['class' => 'btn btn-primary']])
            ->getForm();
        // form - eg. correct category?
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entry->hash = base64_encode(random_bytes(10));
            $entry->dateAdded = new \DateTime();
            $entry->ip = $request->getClientIp();
            $entry->userAgent = $request->headers->get('User-Agent');

            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($entry);
            $dm->flush();


            $ret = $this->sendConfirmationEmail($entry, $price, $totalPrice);
            // remove from session
            $request->getSession()->remove('entry');
            // redirect to hooray
            return $this->redirectToRoute('entry-hooray');
        }


        return $this->render('default/recapitulation.html.twig',
            ['form' => $form->createView(),
                'entry' => $entry, 'prices' => $price, 'totalPrice' => $totalPrice]
        );
    }

    /**
     * @Route("/prihlaska-hotovo", name="entry-hooray")
     */
    public function hoorayAction()
    {
        return $this->render('default/hooray.html.twig');
    }

    /**
     * @Route("/prihlasky-seznam", name="entry-list")
     */
    public function listAction()
    {
        $dm = $this->get('doctrine_mongodb')->getManager();
        $entries = $dm->getRepository(Entry::class)->findAll();

        return $this->render('default/list.html.twig', ['entries' => $entries]);
    }


    /**
     * @Route("/prihlasky", name="prihlasky")
     */
    public function prihlaskyAction(Request $request)
    {

        $emailSubscribe = new EmailSubscribe();

        $form = $this->createFormBuilder($emailSubscribe)
            ->add('email', EmailType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $emailSubscribe->dateAdded = new \DateTime();
            $emailSubscribe->ip = $request->getClientIp();
            $emailSubscribe->userAgent = $request->headers->get('User-Agent');

            // insert into mongo
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($emailSubscribe);
            $dm->flush();


            // add to mailchimp
            $listId = '343249691a';
            $mailChimp = $this->get('mailchimp');
            $mailChimp->verify_ssl = false;
            $ret = $mailChimp->post("lists/$listId/members", [
                'email_address' => $emailSubscribe->email,
                'status' => 'subscribed',
                'language' => $request->getLocale()
            ]);

            if ($ret === false) {
                $this->get('logger')->error('Mailchimp error', ['err' => $mailChimp->getLastError()]);
            }

            $this->addFlash(
                'success',
                'Děkujeme za váš zájem, na email ' . $emailSubscribe->email . ' vás budeme informovat o spuštění přihlášek a dalších událostech'
            );
            return $this->redirectToRoute('prihlasky');
        }

        return $this->render('default/prihlasky.html.twig', ['form' => $form->createView()]);
    }

    private function sendConfirmationEmail(Entry $entry, $price, $totalPrice)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Potvrzení přihlášky na MČR HROB 2016')
            ->setFrom('hrob@ob-lbe.cz')
            ->setBcc('hrob@ob-lbe.cz')
            ->setTo($entry->email)
            ->setBody(
                $this->renderView(
                    ':default:email.txt.twig',
                    ['entry' => $entry, 'prices' => $price, 'totalPrice' => $totalPrice]
                ),
                'text/plain'
            );

        $ret = $this->get('mailer')->send($message);
        if (!$ret) {
            $this->get('logger')->error("Not able to send email", ['entry' => $entry, 'email' => $entry->email]);
            return false;
        }
        return true;
    }
}
