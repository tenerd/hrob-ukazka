FROM php:5-apache
ADD apache2.conf /etc/apache2/apache2.conf
ADD symfony.ini /usr/local/etc/php/conf.d/symfony.ini

RUN apt-get update \
        && apt-get install -y libicu-dev \
        && apt-get clean \
        && rm -rf /var/lib/apt/lists/*

RUN curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer

RUN docker-php-ext-install pdo pdo_mysql opcache intl && a2enmod rewrite && mkdir /var/www/html/web

RUN apt-get update && apt-get install -y \
# redis for session
        php5-redis \
# for mongo ext
        libssl-dev \
# for composer
        git \
    && pecl install mongo \
    && docker-php-ext-enable mongo \
    && rm -r /var/lib/apt/lists/*

ADD . /var/www/html

# Add your application build steps here, for example:

# RUN php /var/www/html/bin/console doctrine:mongodb:schema:create
# copy prod params
RUN cp app/config/parameters.yml.prod app/config/parameters.yml \
# composer install
    && cd /var/www/html/ && composer install -n \
# warmup cache
    && /var/www/html/bin/console cache:warmup -e prod \
# setup rights for cache, logs, and sessions
    && chown -R www-data:www-data /tmp/cache /tmp/logs \
# remove dev files
    && rm -rf /var/www/html/web/app_dev.php \
    && rm -rf /var/www/html/web/config.php